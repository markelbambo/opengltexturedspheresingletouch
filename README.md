# OpenGlTexturedSphereSingleTouch

Fork of OpenGlTexturedSphere, added single touch control to this so the sphere can be rotated.

See [my website](http://www.jimscosmos.com/code/android-open-gl-texture-mapped-spheres/) for details.

I followed [this excellent tutorial from Vogella](http://www.vogella.com/tutorials/AndroidTouch/article.html).

## Release history

### V1.0 - May 2016
Initial version.